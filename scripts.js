var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
redTitle = "####";

function updatePreviewCode(){
	var title, in_app_title;
	if($("#table_title").val() == "") {
		$("#p_title").html("No Title");
		title = "No Title";
	} else {
		$("#p_title").html($("#table_title").val());
		title = $("#table_title").val();
	}
	if($("#in_app_title").val() == "") {
		in_app_title = "No Title";
	} else {
		in_app_title = $("#in_app_title").val();
	}
	$("#in_app_title").val()
	var html = "<table>";
	if($("#red_title").is(":checked")){
		var code = "#### " + title + "\n\n";
	} else {
		var code = "## " + title + "\n\n";
	}
	var textAlignment = "";
	var rows = $("#match_count").val();
	for(var i = -1; i < rows; i++){
		if (i == -1){
			html += "<tr><th>#</th><th>Team 1</th><th>vs.</th><th>Team 2</th>";
			code += "| # | Team 1 | vs. | Team 2 ";
			textAlignment += "| :--: | --: | :--: | :-- ";
			if($("#twitch").is(":checked")){
				html += "<th>Twitch</th><th>Twitch</th>";
				code += "| Twitch | Twitch ";
				textAlignment += "| :--: | :--: ";
			}
			if($("#youtube").is(":checked")){
				html += "<th>Youtube</th><th>Youtube</th>";
				code += "| Youtube | Youtube ";
				textAlignment += "| :--: | :--: ";
			}
			if($("#interview").is(":checked")){
				html += "<th>Interview</th>";
				code += "| Interview ";
				textAlignment += "| :--: ";
			}
			if($("#highlights").is(":checked")){
				html += "<th>Highlights</th>";
				code += "| Highlights ";
				textAlignment += "| :--: ";
			}
			html += "</tr>";
			code += "\n" + textAlignment + "\n";
		} else {
			var bon = $("input[name='bo_match_" + alphabet.charAt(i) + "']").val();
			if ($("input[name='bo_match_" + alphabet.charAt(i) + "']").is('[readonly]') || bon == undefined){
				bon = 1;
			}
			var twTS_pb = $("input[name='tw_ts_pb_match_" + alphabet.charAt(i) + "']").val();
			if ($("input[name='tw_ts_pb_match_" + alphabet.charAt(i) + "']").is('[readonly]') || twTS_pb == undefined){
				twTS_pb = "00h00m00s";
			}
			var twTS_gs = $("input[name='tw_ts_gs_match_" + alphabet.charAt(i) + "']").val();
			if ($("input[name='tw_ts_gs_match_" + alphabet.charAt(i) + "']").is('[readonly]') || twTS_gs == undefined){
				twTS_gs = "00h00m00s";
			}
			var ytTS_pb = $("input[name='yt_ts_pb_match_" + alphabet.charAt(i) + "']").val();
			if ($("input[name='yt_ts_pb_match_" + alphabet.charAt(i) + "']").is('[readonly]') || ytTS_pb == undefined){
				ytTS_pb = "00h00m00s";
			}
			var ytTS_gs = $("input[name='yt_ts_gs_match_" + alphabet.charAt(i) + "']").val();
			if ($("input[name='yt_ts_gs_match_" + alphabet.charAt(i) + "']").is('[readonly]') || ytTS_gs == undefined){
				ytTS_gs = "00h00m00s";
			}
			var ytTS_int = $("input[name='int_ts_match_" + alphabet.charAt(i) + "']").val();
			if ($("input[name='int_ts_match_" + alphabet.charAt(i) + "']").is('[readonly]') || ytTS_int == undefined){
				ytTS_int = "00h00m00s";
			}
			if ($.isNumeric(bon)){
				for(var j = 1; j <= bon; j++){
					html += "<tr>";
					if (bon == 1){
						html += "<th>" + alphabet.charAt(i) + "</th>";
						if (i == 0){
							code += "| " + alphabet.charAt(i) + ' [](http://www.table_title.com "' + in_app_title + '")';
						} else {
							code += "| " + alphabet.charAt(i) + " ";
						}
					} else {
						html += "<th>" + alphabet.charAt(i) + j + "</th>";
						if (i == 0 && j == 1){
							code += "| " + alphabet.charAt(i) + j + ' [](http://www.table_title.com "' + in_app_title + '")';
						} else {
							code += "| " + alphabet.charAt(i) + j + " ";
						}
					}
					var winner = "TBD";
					html += "<th>" + $("#l_match" + alphabet.charAt(i)).children("option").filter(":selected").text() + "</th>";
					if ($("#l_match" + alphabet.charAt(i)).val().indexOf("o_team") != -1 || $("#l_match" + alphabet.charAt(i)).val() == "none"){
						code += "| **" + $("#l_match" + alphabet.charAt(i)).children("option").filter(":selected").text() + "** ";
					} else {
						code += "| **[" + $("#l_match" + alphabet.charAt(i)).children("option").filter(":selected").text() + '](/s "' + winner + '")** ';
					}
					html += "<th>vs.</th>";
					code += "| vs. ";
					html += "<th>" + $("#r_match" + alphabet.charAt(i)).children("option").filter(":selected").text() + "</th>";
					if ($("#r_match" + alphabet.charAt(i)).val().indexOf("o_team") != -1 || $("#r_match" + alphabet.charAt(i)).val() == "none"){
						code += "| **" + $("#r_match" + alphabet.charAt(i)).children("option").filter(":selected").text() + "** ";
					} else {
						code += "| **[" + $("#r_match" + alphabet.charAt(i)).children("option").filter(":selected").text() + '](/s "' + winner + '")** ';
					}
					if($("#is_spoiler_match_" + alphabet.charAt(i)).is(":checked")){
						var w_tag = $("input[name='is_spoiler_match_" + alphabet.charAt(i) + "']").val();
						if($("#twitch").is(":checked")){
							html += "<th>Not Streamed</th><th>Winner:<b>" + w_tag + "</b></th>";
							code += '| **[Not Streamed](/s "Not Streamed")** ' + "| [Winner:**" + w_tag + "**](/spoiler) ";
						}
						if($("#youtube").is(":checked")){
							html += "<th>Not Streamed</th><th>Winner:<b>" + w_tag + "</b></th>";
							code += '| **[Not Streamed](/s "Not Streamed")** ' + "| [Winner:**" + w_tag + "**](/spoiler) ";
						}
						if($("#interview").is(":checked")){
							html += "<th>Not Streamed</th>";
							code += '| **[Not Streamed](/s "Not Streamed")** ';
						}
						if($("#highlights").is(":checked")){
							html += "<th>Not Streamed</th>";
							code += '| **[Not Streamed](/s "Not Streamed")** ';
						}
						html += "</tr>";
						code += "\n";
					} else {
						if($("#twitch").is(":checked")){
							html += "<th>Picks & Bans</th><th>Game Start</th>"; //
							code += "| [Picks & Bans](http://www.twitch.tv/riotgames/b/" + $("input[name='twitch']").val() + "/?t=" + twTS_pb + ") " + "| [Game Start](http://www.twitch.tv/riotgames/b/" + $("input[name='twitch']").val() + "/?t=" + twTS_gs + ")";
						}
						if($("#youtube").is(":checked")){
							html += "<th>Picks & Bans</th><th>Game Start</th>";
							code += "| [Picks & Bans](http://www.youtube.com/watch?v=" + $("input[name='youtube']").val() + "&t=" + ytTS_pb + ") " + "| [Game Start](http://www.youtube.com/watch?v=" + $("input[name='youtube']").val() + "&t=" + ytTS_gs + ")";
						}
						if($("#interview").is(":checked")){
							html += "<th>Interview</th>";
							code += "| [Interview](http://www.youtube.com/watch?v=" + $("input[name='int_id_match_" + alphabet.charAt(i) + "']").val() + "&t=" + ytTS_int + ") ";
						}
						if($("#highlights").is(":checked")){
							html += "<th>Highlights</th>";
							code += "| [Highlights](http://www.youtube.com/watch?v=" + $("input[name='hl_match_" + alphabet.charAt(i) + "']").val() + ") ";
						}
						html += "</tr>";
						code += "\n";
					}
				}
			} else {
				return false;
			}
		}
	}
	html += "</table>";
	$("#p_table").html(html);
	$("#p_code").text(code);
}

function updateCustomization(){
	var tags = new Array();
	for(var i = 1; i <= $("div[id='c_team_names'] input[type='text']").length; i++){
		tags[i] = $("div[id='c_team_names'] input[id*='" + i + "']").val();
	}
	var team1 = new Array();
	var team2 = new Array();
	for(var i = 0; i < $("#c_matches_setup > div[class='sided_table_row']").length; i++){
			team1[i] = $("#l_match" + alphabet.charAt(i)).val();
			team2[i] = $("#r_match" + alphabet.charAt(i)).val();
	}
	/*var eo = "";
	var settings = new Array();
	for(var i = 1; i <= $("div[id='c_team_names'] input[type='text']").length; i++){
		tags[i] = $("div[id='c_team_names'] input[id*='" + i + "']").val();
	}*/
	
	if($.trim($("#team_count").val()) == ""){
	$("#team_count").text(0);
	$("#c_team_names").html("");
	} else if($.isNumeric($("#team_count").val())) {
		if ($("#team_count").val() > 0){
			$("#c_team_names").html("");
			for(var i = 1; i <= $("#team_count").val(); i++){
				if (tags[i] == undefined){
					tags[i] = "";
				}
				$("#c_team_names").append("<label>Team Tag: <input type='text' onchange='updateMatchDD();updatePreviewCode();' id='team" + i + "' value='" + tags[i] + "'size='4'></input></label>&nbsp;&nbsp;&nbsp;&nbsp;");
				if (i % 2 == 0){
					$("#c_team_names").append("<br />");
				}
			}
		} else {
		alert("The number of teams must be positive");
		return false;
		}
	} else {
		alert("The number of teams is invalid.");
		return false;
	}
	if($.trim($("#match_count").val()) == ""){
	$("#match_count").val("0");
	$("#c_matches_setup").html("");
	} else if($.isNumeric($("#match_count").val())){
		if ($("#match_count").val() > 0){
			$("#c_matches_setup").html("");
			for(var i = 0; i < $("#match_count").val(); i++){
				var html = "";
				html += "<div class='sided_table_row'>";
				html += "<div class='sided_table_cell'>";
				html += "<p>Match " + alphabet.charAt(i) + ": <select id='l_match" + alphabet.charAt(i) + "' onchange='updatePreviewCode()'>";
				html += "<option value='none'>TEAM 1</option>";
				for(var j = 1; j <= $("#team_count").val(); j++){
					html += "<option value='o_team" + j + "'>" + $("#team"+j).val() + "</option>";
				}
				for(var m = 0; m < $("#match_count").val(); m++){
					if(m != (i)){
						html += "<option value='o_match" + alphabet.charAt(m) + "'>" + "Winner of " + alphabet.charAt(m) + "</option>"
					}
				}
				html += "</select>" + "&nbsp;&nbsp;vs.&nbsp;&nbsp;";
				html += "<select id='r_match" + alphabet.charAt(i) + "' onchange='updatePreviewCode()'>";
				html += "<option value='none' selected='selected'>TEAM 2</option>";
				for(var k = 1; k <= $("#team_count").val(); k++){
					html += "<option value='o_team" + k + "'>" + $("#team" + k).val() + "</option>";
				}
				for(var n = 0; n < $("#match_count").val(); n++){
					if(n != (i)){
						html += "<option value='o_match" + alphabet.charAt(n) + "'>" + "Winner of " + alphabet.charAt(n) + "</option>"
					}
				}
				html += "</select>";
				html += "&nbsp;&nbsp;&nbsp;&nbsp;<label><input type='checkbox' onchange='enableEO(this)' id='eo_match_" + alphabet.charAt(i) + "'></input>Show Extra Options</label>";
				html += "</p>";
				html += "</div>";
				html += "</div>";

				html += "<div name='eo_match_" + alphabet.charAt(i) + "' class='sided_table borderHidden'>";
				
				html += "<div id='bo_eo' class='sided_table'>";
				
				html += "<div class='sided_table_cell'>";
				html += "<label><input type='checkbox' onchange='toggleField(this)' id='bo_match_" + alphabet.charAt(i) + "'></input>Best Of&nbsp;&nbsp;</label>";
				html += "<input name='bo_match_"+ alphabet.charAt(i) +"' type='text' size='1' readonly value='1' onchange='updatePreviewCode()'></input>";
				html += "</div>";
				
				html += "</div>";
				
				html += "<div id='tw_eo' class='sided_table'>";
				
				html += "<div class='sided_table_cell'>";
				html += "<label><input type='checkbox' onchange='toggleField(this)' id='tw_ts_pb_match_" + alphabet.charAt(i) + "'></input>Twitch Timestamp for Picks &amp; Bans&nbsp;&nbsp;</label>";
				html += "<input name='tw_ts_pb_match_"+ alphabet.charAt(i) +"' type='text' size='8' readonly value='00h00m00s' onchange='updatePreviewCode()'></input>";
				html += "</div>"
				
				html += "<div class='sided_table_cell'>";
				html += "<label><input type='checkbox' onchange='toggleField(this)' id='tw_ts_gs_match_" + alphabet.charAt(i) + "'></input>Twitch Timestamp for Game Start&nbsp;&nbsp;</label>";
				html += "<input name='tw_ts_gs_match_"+ alphabet.charAt(i) +"' type='text' size='8' readonly value='00h00m00s' onchange='updatePreviewCode()'></input>";
				html += "</div>";
				
				html += "</div>";
				
				html += "<div id='yt_eo' class='sided_table'>";
				
				html += "<div class='sided_table_cell'>";
				html += "<label><input type='checkbox' onchange='toggleField(this)' id='yt_ts_pb_match_" + alphabet.charAt(i) + "'></input>Youtube Timestamp for Picks &amp; Bans&nbsp;&nbsp;</label>";
				html += "<input name='yt_ts_pb_match_"+ alphabet.charAt(i) +"' type='text' size='8' readonly value='00h00m00s' onchange='updatePreviewCode()'></input>";
				html += "</div>"
				
				html += "<div class='sided_table_cell'>";
				html += "<label><input type='checkbox' onchange='toggleField(this)' id='yt_ts_gs_match_" + alphabet.charAt(i) + "'></input>Youtube Timestamp for Game Start&nbsp;&nbsp;</label>";
				html += "<input name='yt_ts_gs_match_"+ alphabet.charAt(i) +"' type='text' size='8' readonly value='00h00m00s' onchange='updatePreviewCode()'></input>";
				html += "</div>"
				
				html += "</div>";
				
				html += "<div id='int_eo' class='sided_table'>";
				
				html += "<div class='sided_table_cell'>";
				html += "<label><input type='checkbox' onchange='toggleField(this);updateIntVID();' id='int_id_match_" + alphabet.charAt(i) + "'></input>Youtube Video ID for Interview&nbsp;&nbsp;</label>";
				html += "<input name='int_id_match_"+ alphabet.charAt(i) +"' type='text' size='16' readonly value='VIDEO_ID' onchange='updatePreviewCode();'></input>";
				html += "</div>"
				
				html += "<div class='sided_table_cell'>";
				html += "<label><input type='checkbox' onchange='toggleField(this)' id='int_ts_match_" + alphabet.charAt(i) + "'></input>Youtube Timestamp for Interview&nbsp;&nbsp;</label>";
				html += "<input name='int_ts_match_"+ alphabet.charAt(i) +"' type='text' size='8' readonly value='00h00m00s' onchange='updatePreviewCode()'></input>";
				html += "</div>"
				
				html += "</div>";
				
				html += "<div id='hl_eo' class='sided_table'>";
				
				html += "<div class='sided_table_cell'>";
				html += "<label><input type='checkbox' onchange='toggleField(this)' id='hl_match_" + alphabet.charAt(i) + "'></input>Youtube Video ID for Highlights&nbsp;&nbsp;</label>";
				html += "<input name='hl_match_"+ alphabet.charAt(i) +"' type='text' size='16' readonly value='HIGHLIGHTS_VID' onchange='updatePreviewCode()'></input>";
				html += "</div>"
				
				html += "</div>";
				
				html += "<div id='spoiler_eo' class='sided_table'>";
				
				html += "<div class='sided_table_cell'>";
				html += "<label><input type='checkbox' onchange='toggleField(this)' id='is_spoiler_match_" + alphabet.charAt(i) + "'></input>Is this match a spoiler holder?&nbsp;&nbsp;</label>";
				html += "<input name='is_spoiler_match_"+ alphabet.charAt(i) +"' type='text' size='16' readonly value='WINNER_TAG' onchange='updatePreviewCode()'></input>";
				html += "</div>"
				
				html += "</div>";
				
				html += "</div>";
				html += "</div>";
				$("#c_matches_setup").append(html);
				if(typeof team1[i] == "undefined"){
					team1[i] = "none";
				}
				if(typeof team2[i] == "undefined"){
					team2[i] = "none";
				}
				$("#l_match" + alphabet.charAt(i)).val(team1[i]);
				$("#r_match" + alphabet.charAt(i)).val(team2[i]);
			}
		} else {
		alert("The number of matches must be positive");
		return false;
		}
	} else {
		alert("The number of matches is invalid.");
		return false;
	}
	updatePreviewCode();
}

function toggleField(elem){
	if ($("input[name='" + $(elem).attr("id") + "']").is('[readonly]')){
		$("input[name='" + $(elem).attr("id") + "']").attr('readonly', false);
	} else {
		$("input[name='" + $(elem).attr("id") + "']").attr('readonly', true);
	}
	updatePreviewCode();
}

function updateEO(){

}

function updateIntVID(){
	for(var i = 0; i < $("#match_count").val(); i++){
		if($("input[name='int_id_match_" + alphabet.charAt(i) + "']").is('[readonly]')){
			$("input[name='int_id_match_" + alphabet.charAt(i) + "']").val($("input[name='youtube']").val());
		}
	}
}

function enableEO(id){
	if ($("div[name='" + $(id).attr("id") + "']").css('display') == 'block'){
		$("div[name='" + $(id).attr("id") + "']").css('display', 'none');
	} else {
		$("div[name='" + $(id).attr("id") + "']").css('display', 'block');
	}
	updatePreviewCode();
}

function updateMatchDD(){
	for(var j = 1; j <= $("#team_count").val(); j++){
		$("option[value=o_team" + (j)).text($("#team" + (j)).val());
	}
}

function showTableTip(){
document.getElementById("table_tip").style.display = "block";
document.getElementById("help_tip").style.display = "none";
}
function hideTableTip(){
document.getElementById("table_tip").style.display = "none";
document.getElementById("help_tip").style.display = "block";
}
function showTeamsTip(){
document.getElementById("teams_tip").style.display = "block";
document.getElementById("help_tip").style.display = "none";
}
function hideTeamsTip(){
document.getElementById("teams_tip").style.display = "none";
document.getElementById("help_tip").style.display = "block";
}
function showMatchesTip(){
document.getElementById("matches_tip").style.display = "block";
document.getElementById("help_tip").style.display = "none";
}
function hideMatchesTip(){
document.getElementById("matches_tip").style.display = "none";
document.getElementById("help_tip").style.display = "block";
}
function showColumnsTip(){
document.getElementById("columns_tip").style.display = "block";
document.getElementById("help_tip").style.display = "none";
}
function hideColumnsTip(){
document.getElementById("columns_tip").style.display = "none";
document.getElementById("help_tip").style.display = "block";
}
function showCustomizationTip(){
document.getElementById("customization_tip").style.display = "block";
document.getElementById("help_tip").style.display = "none";
}
function hideCustomizationTip(){
document.getElementById("customization_tip").style.display = "none";
document.getElementById("help_tip").style.display = "block";
}
function showPreviewTip(){
document.getElementById("preview_tip").style.display = "block";
document.getElementById("help_tip").style.display = "none";
}
function hidePreviewTip(){
document.getElementById("preview_tip").style.display = "none";
document.getElementById("help_tip").style.display = "block";
}
function showCodeTip(){
document.getElementById("code_tip").style.display = "block";
document.getElementById("help_tip").style.display = "none";
}
function hideCodeTip(){
document.getElementById("code_tip").style.display = "none";
document.getElementById("help_tip").style.display = "block";
}

function pageStart(){
	updateCustomization();
	updatePreviewCode();
}

$(document).ready(function(){
	pageStart();
});